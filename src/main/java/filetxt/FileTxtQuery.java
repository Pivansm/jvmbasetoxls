package filetxt;

import java.io.*;

public class FileTxtQuery {
    private String fileName;
    private String queryTxt;

    public FileTxtQuery() {

    }

    public FileTxtQuery(String fileName) {
        this.fileName = fileName;
    }

    private void getFileQuery(){
        queryTxt = "";
        File file = new File(fileName);
        if(file.exists()) {
            System.out.println("Файл в обработке:" + file.getName());
            try (BufferedReader b = new BufferedReader(new InputStreamReader(new FileInputStream(file), "CP1251"));)
               {
                String readLine = "";
                while ((readLine = b.readLine()) != null) {
                    queryTxt += readLine;
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            queryTxt = null;
        }
    }

    public String getQueryTxt() {
        getFileQuery();
        return queryTxt;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
