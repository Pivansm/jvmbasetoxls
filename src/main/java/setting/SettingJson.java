package setting;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class SettingJson  extends AbstractJson<Setting> {
    private static Logger loggerSTG = LogManager.getLogger(AppConfig.class);

    @Override
    public Setting findEntityBy() {
        Setting setting;
        ObjectMapper mapper = new ObjectMapper();

        File file = new File("ThisSetting.json");

        try(BufferedReader reader = new BufferedReader(new FileReader(file)))
        {
            setting = mapper.readValue(reader, Setting.class);
            return setting;
        }
        catch (IOException ex2)
        {
            System.out.println("Err:=" + ex2.getMessage());
            loggerSTG.error(ex2.getMessage(), ex2);
            return null;
        }
    }

    @Override
    public boolean create() {
        Setting setting = new Setting();
        setting.setQueryName("zapros1.txt");
        setting.setTypeServer("oracle");
        setting.setServerName("AMK");
        setting.setScriptSize(500);

        ObjectMapper mapper = new ObjectMapper();

        File file = new File("ThisSetting.json");
        if (!file.exists()) {

            try (FileWriter writer = new FileWriter(file, true)) {
                mapper.writeValue(writer, setting);
                return true;
            } catch (IOException ex) {
                System.out.println("Err" + ex.getMessage());
                return false;
            }
        }

        return false;

    }
}
