package setting;

public abstract class AbstractJson<T> {
    public abstract T findEntityBy();
    public abstract boolean create();
}
