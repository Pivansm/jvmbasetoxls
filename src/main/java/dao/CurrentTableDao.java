package dao;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;

public interface CurrentTableDao {
    ResultSet findByAll(String query);
    ResultSet findByAllPrSt(String query);
    DatabaseMetaData getMetaData();
    String fieldsParameter(String query, String tableName);
    void runnerToQuery(String query);
    Long getRowCount(String query);
}
