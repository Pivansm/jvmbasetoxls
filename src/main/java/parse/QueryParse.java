package parse;

import dao.impl.TableAllDaoJdbcImpl;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class QueryParse {
    private static final String MULTI_PARAM_FL = "@TFL1";
    private static final String MULTI_PARAM_TMFL = "@TMFL1";
    private static final String FILTER_WHERE = "WHERE";
    private static final String TRUNC_TMP_TBL = "BEGIN EXECUTE IMMEDIATE 'TRUNCATE TABLE PERMYAKOV_IS.INN_NOTMP'; END; ";
    private static final String INSERT_TMP_TBL = "INSERT INTO PERMYAKOV_IS.INN_NOTMP (INN) VALUES (?) ";

    private String query;
    private List<String> queryList;
    private TableAllDaoJdbcImpl oracleDaoJdbc;

    public QueryParse(String query) {
        this.query = query;
        queryList = new ArrayList<>();
    }

    public QueryParse(TableAllDaoJdbcImpl oracleDaoJdbc, String query) {
        this.query = query;
        queryList = new ArrayList<>();
        this.oracleDaoJdbc = oracleDaoJdbc;
    }

    private void parseQuery() {
        HashSet<String> hashSet = new HashSet<>();
        Pattern pattern = Pattern.compile(MULTI_PARAM_FL);
        Matcher matcher = pattern.matcher(query);

        if(matcher.find()) {
            System.out.println("Найден мак:" + matcher.group(0));
            JFileChooser fileChooser = new JFileChooser();
            int ret = fileChooser.showDialog(null, "Открыть файл");
            if(ret == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                System.out.println("Файл в обработке:" + file.getName());
                try{
                    BufferedReader b = new BufferedReader(new FileReader(file));
                    String readLine = "";
                    while((readLine = b.readLine()) != null) {
                        hashSet.add(readLine);
                    }

                    String strMacros = hashSet.stream()
                            .collect(Collectors.joining("', '"));
                    query = query.replace(MULTI_PARAM_FL, "'" + strMacros + "'");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void parseQueryMulti(int iCountRec) {
        HashSet<String> hashSet = new HashSet<>();
        Pattern pattern = Pattern.compile(MULTI_PARAM_FL);
        Matcher matcher = pattern.matcher(query);
        boolean flgEmpty = true;
        if(iCountRec > 1000) { iCountRec = 1000; }
        if(matcher.find()) {
            System.out.println("Найден мак:" + matcher.group(0));
            JFileChooser fileChooser = new JFileChooser();
            int ret = fileChooser.showDialog(null, "Открыть файл");
            if(ret == JFileChooser.APPROVE_OPTION) {
                flgEmpty = false;
                File file = fileChooser.getSelectedFile();
                System.out.println("Файл в обработке:" + file.getName());
                try{
                    BufferedReader b = new BufferedReader(new FileReader(file));
                    String readLine = "";
                    while((readLine = b.readLine()) != null) {
                        hashSet.add(readLine);
                    }

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                int countRow = 1;
                HashSet<String> hashSetProm = new HashSet<>();
                for (String row: hashSet) {
                    hashSetProm.add(row);
                    if(countRow%iCountRec == 0) {
                        String strMacros = hashSetProm.stream()
                                .collect(Collectors.joining("', '"));
                        queryList.add(query.replace(MULTI_PARAM_FL, "'" + strMacros + "'"));
                        hashSetProm = new HashSet<>();
                    }
                    countRow++;
                }

                String strMacros = hashSetProm.stream()
                        .collect(Collectors.joining("', '"));
                queryList.add(query.replace(MULTI_PARAM_FL, "'" + strMacros + "'"));
                hashSetProm = new HashSet<>();
                countRow--;
                System.out.println("Количество строк в файле:" + countRow);
            }
        }

        pattern = Pattern.compile(MULTI_PARAM_TMFL);
        matcher = pattern.matcher(this.query);
        if (matcher.find()) {
            System.out.println("Найден мак:" + matcher.group(0));
            JFileChooser fileChooser = new JFileChooser();
            int ret = fileChooser.showDialog(null, "Открыть файл");
            if (ret == JFileChooser.APPROVE_OPTION) {
                flgEmpty = false;
                File file = fileChooser.getSelectedFile();
                System.out.println("Файл в обработке:" + file.getName());
                try{
                    BufferedReader b = new BufferedReader(new InputStreamReader(new FileInputStream(file), "CP1251"));
                    String readLine = "";
                    while((readLine = b.readLine()) != null) {
                        hashSet.add(readLine);
                    }

                    insertToTmpTblOracle(hashSet);
                    queryList.add(query.replace(MULTI_PARAM_TMFL, "INN"));

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

        if (flgEmpty) {
            this.queryList.add(this.query);
        }

    }

    public String getQuery() {
        parseQuery();
        return query;
    }

    public List<String> getQueryMulti(int iCountRec) {
        queryList = new ArrayList<>();
        parseQueryMulti(iCountRec);
        return queryList;
    }

    public String parseQueryWhere() {
        String outQuery = this.query;
        Pattern pattern = Pattern.compile(FILTER_WHERE);
        Matcher matcher = pattern.matcher(outQuery);
        if(matcher.find()) {
            int matcherEnd = 0;
            while (matcher.find()) {
                matcherEnd = matcher.end();
            }
            //System.out.println(matcherEnd);
            outQuery = outQuery.substring(0, matcherEnd);
        }
        outQuery = outQuery + " 1=1 AND ROWNUM <= 10 ";
        //System.out.println(outQuery);
        return outQuery;
    }

    public void insertToTmpTblOracle(HashSet<String> hashSet)
    {
        //
        oracleDaoJdbc.runnerToQuery(TRUNC_TMP_TBL);
        //
        oracleDaoJdbc.insertBatchVarchar2(hashSet, INSERT_TMP_TBL, 1000);
    }




}
