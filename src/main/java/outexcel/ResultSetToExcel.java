package outexcel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import setting.AppConfig;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ResultSetToExcel {
    private static Logger loggerXLS = LogManager.getLogger(AppConfig.class);
    private String fileName;
    private String nameSheet;
    private SXSSFWorkbook workbook;
    private ResultSet resultSet;
    private Sheet sheet;
    private int iRow;
    private int iCollSheet;
    private boolean bFlagHeader;

    public ResultSetToExcel() {
        this.iRow = 1;
        this.iCollSheet = 1;
        this.bFlagHeader = false;
    }

    public ResultSetToExcel(String fileName) {
        this.fileName = fileName;
        this.iRow = 1;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public void setNameSheet(String nameSheet) {
        this.nameSheet = nameSheet;
    }

    public String getNameSheet() {
        if (nameSheet == null || nameSheet.equals("")) {
            nameSheet = "Report" + iCollSheet;
        }
        return nameSheet;
    }

    private void deleteFile() {
        File currSqlite = new File(fileName);
        if (currSqlite.exists()) {
            currSqlite.delete();
        }
    }

    private List<String> getHeaderNames() {
        List<String> names = new ArrayList<>();
        try {
            int columns = getCountColumn();
            ResultSetMetaData metaData = resultSet.getMetaData();
            for (int i = 0; i < columns; i++) {
                names.add(metaData.getColumnName(i + 1));
            }
            return names;
        } catch (SQLException e) {
            e.printStackTrace();
            loggerXLS.error(e.getMessage(), e);
            return null;
        }
    }

    private int getCountColumn() {
        try {
            return resultSet.getMetaData().getColumnCount();
        } catch (SQLException e) {
            e.printStackTrace();
            loggerXLS.error(e.getMessage(), e);
            return -1;
        }
    }

    public void writeIntoExcelSXSSF() {
        //
        deleteFile();
        try {
            FileOutputStream stream = new FileOutputStream(fileName);
            //workbook
            workbook = new SXSSFWorkbook(-1);
            Sheet sheet = workbook.createSheet(getNameSheet());
            Row row;
            Cell cell;
            //
            int columns = getCountColumn();
            //Заполняем загаловка таблицы
            List<String> names = getHeaderNames();
            createHeaderSheet(names, sheet, columns);
            //
            iRow = 1;
            resultSet.setFetchSize(100);
            while (resultSet.next()) {
                row = sheet.createRow(iRow);
                for (int j = 0; j <= columns - 1; j++) {
                    cell = row.createCell(j);
                    //String address = new CellReference(cell).formatAsString();
                    cell.setCellValue(resultSet.getString(j + 1));
                }
                iRow++;
                if (iRow % 2000 == 0) {
                    System.out.println("Записей: " + iRow);
                    ((SXSSFSheet) sheet).flushRows(2000);
                }
                if(iRow % 1000000 == 0) {
                    iRow = 1;
                    iCollSheet++;
                    setNameSheet("Report" + iCollSheet);
                    sheet = workbook.createSheet(getNameSheet());
                    createHeaderSheet(names, sheet, columns);
                    System.out.println("Новый лист:= " + getNameSheet());
                }
            }
            //
            System.out.println("Создание файла: " + fileName);
            workbook.write(stream);
            stream.close();
            workbook.dispose();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            loggerXLS.error(throwables.getMessage(), throwables);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            loggerXLS.error(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
            loggerXLS.error(e.getMessage(), e);
        }
    }


    public void writeIntoExcelSXSSFMulti() {
        //
        Row row;
        Cell cell;
        int columns = getCountColumn();
        //
        try {
            resultSet.setFetchSize(100);
            while (resultSet.next()) {
                row = sheet.createRow(iRow);
                for (int j = 0; j <= columns - 1; j++) {
                    cell = row.createCell(j);
                    //String address = new CellReference(cell).formatAsString();
                    cell.setCellValue(resultSet.getString(j + 1));
                }
                iRow++;
                if (iRow % 2000 == 0) {
                    System.out.println("Записей: " + iRow);
                    ((SXSSFSheet) sheet).flushRows(2000);
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            loggerXLS.error(throwables.getMessage(), throwables);
        } catch (IOException e) {
            e.printStackTrace();
            loggerXLS.error(e.getMessage(), e);
        }
    }

    public void openBook() {
        workbook = new SXSSFWorkbook(-1);
        sheet = workbook.createSheet(getNameSheet());
    }

    public void createHeaderSheet() {
        if(!bFlagHeader) {
            Cell cell;
            int columns = getCountColumn();
            //Заполняем загаловка таблицы
            List<String> names = getHeaderNames();
            int i = 0;
            Row row = sheet.createRow(0);
            for (String headCell : names) {
                cell = row.createCell(i);
                cell.setCellValue(headCell);
                i++;
            }
            bFlagHeader = true;
        }
    }


    public void createHeaderSheet(List<String> names, Sheet sheet, int columns) {
            Cell cell;
            //Заполняем загаловка таблицы
            int i = 0;
            Row row = sheet.createRow(0);
            for (String headCell : names) {
                cell = row.createCell(i);
                cell.setCellValue(headCell);
                i++;
            }
    }


    public boolean writeBook() {
        deleteFile();
        try {
            FileOutputStream stream = new FileOutputStream(fileName);
            //
            workbook.write(stream);
            stream.close();
            workbook.dispose();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public int getiRow() {
        return iRow;
    }
}
