package setting;

public class Setting {
    private String queryName;
    private String typeServer;
    private String serverName;
    private int scriptSize;

    public String getQueryName() {
        return queryName;
    }

    public void setQueryName(String queryName) {
        this.queryName = queryName;
    }

    public int getScriptSize() {
        return scriptSize;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public void setScriptSize(int scriptSize) {
        this.scriptSize = scriptSize;
    }

    public String getTypeServer() {
        return typeServer;
    }

    public void setTypeServer(String typeServer) {
        this.typeServer = typeServer;
    }
}
