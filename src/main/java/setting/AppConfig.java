package setting;

import dao.impl.TableAllDaoJdbcImpl;
import filetxt.FileTxtQuery;
import oracle.jdbc.OracleConnection;
import oracle.net.ano.AnoServices;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import runner.MainLaunch;

import javax.sql.DataSource;
import java.sql.Driver;
import java.util.Properties;

@Configuration
@PropertySource("classpath:dataSource.properties")
public class AppConfig {
    private static Logger loggerCFG = LogManager.getLogger(AppConfig.class);
    @Value("${oracle.serverName}")
    private String serverName;
    @Value("${oracle.driverClassName}")
    private String driverClassName;
    @Value("${oracle.url}")
    private String url;
    @Value("${oracle.username}")
    private String username;
    @Value("${oracle.password}")
    private String password;

    @Value("${mssqlu1.driverClassName}")
    private String driverClassNameMssql;
    @Value("${mssqlu1.url}")
    private String urlMssql;
    @Value("${mssqlu1.username}")
    private String usernameMssql;
    @Value("${mssqlu1.password}")
    private String passwordMssql;

    @Value("${mssqlu2.driverClassName}")
    private String driverClassNameMssql_u2;
    @Value("${mssqlu2.url}")
    private String urlMssql_u2;
    @Value("${mssqlu2.username}")
    private String usernameMssql_u2;
    @Value("${mssqlu2.password}")
    private String passwordMssql_u2;

    @Bean
    public MainLaunch mainLaunch() {
        MainLaunch mainLaunch = new MainLaunch();
        mainLaunch.setSetting(settingUp());
        mainLaunch.setOracleDaoJdbc(oracleDaoJdbc());
        mainLaunch.setMssqlDaoJdbc(mssqlDaoJdbc());
        mainLaunch.setFileTxtQuery(fileToQuery());
        return mainLaunch;
    }

    @Bean
    public DataSource dataSourceOracle() {
        try {
            SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
            Class<? extends Driver> driver = (Class<? extends Driver>) Class.forName(driverClassName);

            //Oracle
            Properties props = new Properties();
            props.setProperty(OracleConnection.CONNECTION_PROPERTY_THIN_NET_AUTHENTICATION_SERVICES,
                    "( " + AnoServices.AUTHENTICATION_KERBEROS5 + " )");
            props.setProperty(OracleConnection.CONNECTION_PROPERTY_THIN_NET_AUTHENTICATION_KRB5_MUTUAL, "true");

            dataSource.setConnectionProperties(props);
            dataSource.setDriverClass(driver);
            dataSource.setUrl(url);
            dataSource.setUsername(username);
            dataSource.setPassword(password);
            return dataSource;
        } catch (Exception e) {
            e.printStackTrace();
            loggerCFG.error(e.getMessage(), e);
            return null;
        }
    }


    @Bean
    public DataSource dataSourceMssql() {
        try {
            SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
            Class<? extends Driver> driver = (Class<? extends Driver>) Class.forName(driverClassNameMssql);
            dataSource.setDriverClass(driver);
            dataSource.setUrl(urlMssql);
            return dataSource;
        } catch (Exception e) {
            e.printStackTrace();
            loggerCFG.error(e.getMessage(), e);
            return null;
        }
    }

    @Bean
    public DataSource dataSourceMssql_u2() {
        try {
            SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
            Class<? extends Driver> driver = (Class<? extends Driver>) Class.forName(driverClassNameMssql_u2);
            dataSource.setDriverClass(driver);
            dataSource.setUrl(urlMssql_u2);
            return dataSource;
        } catch (Exception e) {
            e.printStackTrace();
            loggerCFG.error(e.getMessage(), e);
            return null;
        }
    }


    @Bean
    public TableAllDaoJdbcImpl oracleDaoJdbc() {
        TableAllDaoJdbcImpl tableAllDaoJdbc = new TableAllDaoJdbcImpl();
        tableAllDaoJdbc.setDataSource(dataSourceOracle());
        return tableAllDaoJdbc;
    }

    @Bean
    public TableAllDaoJdbcImpl mssqlDaoJdbc() {
        TableAllDaoJdbcImpl tableAllDaoJdbc = new TableAllDaoJdbcImpl();
        tableAllDaoJdbc.setDataSource(dataSourceMssql());
        return tableAllDaoJdbc;
    }

    @Bean
    public Setting settingUp() {
        Setting setting;
        SettingJson settingJson;
        //
        settingJson = new SettingJson();
        settingJson.create();
        setting = settingJson.findEntityBy();
        return setting;
    }

    @Bean
    public FileTxtQuery fileToQuery() {
        FileTxtQuery fileTxtQuery = new FileTxtQuery();
        return fileTxtQuery;
    }

}
