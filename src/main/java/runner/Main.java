package runner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import setting.AppConfig;

import java.sql.SQLException;

public class Main {
    static final Logger rootLogger = LogManager.getRootLogger();

    public static void main(String[] args) {
        rootLogger.info("=======Start========");
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
        MainLaunch mainLaunch = (MainLaunch) applicationContext.getBean("mainLaunch");

        try {
            mainLaunch.processingFoundation();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        System.out.println("=======Finish========");
        rootLogger.info("=======Finish========");
    }
}
