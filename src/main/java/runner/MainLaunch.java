package runner;

import dao.impl.TableAllDaoJdbcImpl;
import filetxt.FileTxtQuery;
import outexcel.ResultSetToExcel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import parse.QueryParse;
import setting.Setting;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class MainLaunch {
    private static Logger loggerML = LogManager.getLogger(MainLaunch.class);
    private Setting setting;
    private TableAllDaoJdbcImpl oracleDaoJdbc;
    private TableAllDaoJdbcImpl mssqlDaoJdbc;
    private FileTxtQuery fileTxtQuery;
    private QueryParse queryParse;
    private String outFileName;

    //Настройки
    public void setSetting(Setting setting) { this.setting = setting; }
    //Oracle
    public void setOracleDaoJdbc(TableAllDaoJdbcImpl oracleDaoJdbc) {
        this.oracleDaoJdbc = oracleDaoJdbc;
    }
    //Mssql
    public void setMssqlDaoJdbc(TableAllDaoJdbcImpl mssqlDaoJdbc) {
        this.mssqlDaoJdbc = mssqlDaoJdbc;
    }
    //FileQuery
    public void setFileTxtQuery(FileTxtQuery fileTxtQuery) { this.fileTxtQuery = fileTxtQuery; }

    public void processingFoundation() throws SQLException {
        //
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("ddMMyyyy");
        String cuDate = LocalDate.now().format(formatter1);

        String tkPath = System.getProperty("user.dir");
        System.out.println(tkPath);
        fileTxtQuery.setFileName(tkPath + "\\" + setting.getQueryName());
        String pQuery = fileTxtQuery.getQueryTxt();
        System.out.println(pQuery);
        //
        outFileName = tkPath + "\\REPORTS\\" + "Excel_" + cuDate + ".xlsx";

        queryParse = new QueryParse(this.oracleDaoJdbc, pQuery);
        //String queryNw = queryParse.getQuery();
        List<String> queryList = queryParse.getQueryMulti(setting.getScriptSize());
        //Oracle TBL
        {
            if(queryList.size() > 1) { //multiQuery
                ResultSetToExcel resultSetToExcel = new ResultSetToExcel();
                resultSetToExcel.setFileName(outFileName);
                resultSetToExcel.openBook();
                for (String qu : queryList) {
                    System.out.println(qu);
                    //
                    try {
                        System.out.println("Используется сервер " + setting.getServerName() + " типа:=" + setting.getTypeServer());
                        ResultSet rsProm;
                        if(setting.getTypeServer().equals("oracle")) {
                            rsProm = oracleDaoJdbc.findByAllPrSt(qu);
                        } else {
                            rsProm = mssqlDaoJdbc.findByAllPrSt(qu);
                        }
                        //
                        resultSetToExcel.setResultSet(rsProm);
                        //Загаловок на странице Excel
                        resultSetToExcel.createHeaderSheet();
                        //Формирование листа
                        resultSetToExcel.writeIntoExcelSXSSFMulti();
                        //
                        rsProm.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                        loggerML.error(e.getMessage(), e);
                    }
                }
                //Запись в файл
                resultSetToExcel.writeBook();
                int iCountRoe = resultSetToExcel.getiRow();
                System.out.println("Всего записей:= " + iCountRoe);
            } else {
                ResultSetToExcel resultSetToExcel = new ResultSetToExcel();
                resultSetToExcel.setFileName(outFileName);
                String qu =  queryList.get(0);
                System.out.println(qu);
                //
                try {
                    System.out.println("Используется сервер " + setting.getServerName() + " типа:=" + setting.getTypeServer());
                    ResultSet rsProm;
                    if(setting.getTypeServer().equals("oracle")) {
                        rsProm = oracleDaoJdbc.findByAllPrSt(qu);
                    } else {
                        rsProm = mssqlDaoJdbc.findByAllPrSt(qu);
                    }
                    //
                    resultSetToExcel.setResultSet(rsProm);
                    resultSetToExcel.writeIntoExcelSXSSF();
                    //
                    int iCountRoe = resultSetToExcel.getiRow();
                    System.out.println("Всего записей:= " + iCountRoe);
                    //
                    rsProm.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    loggerML.error(e.getMessage(), e);
                }

            }

        }
    }

}
