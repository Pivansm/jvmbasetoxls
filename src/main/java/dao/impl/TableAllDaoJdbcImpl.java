package dao.impl;

import dao.CurrentTableDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.HashSet;
import java.util.List;

public class TableAllDaoJdbcImpl implements CurrentTableDao {
    private static Logger loggerTBL = LogManager.getLogger(TableAllDaoJdbcImpl.class);
    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public ResultSet findByAll(String query) {
        try {
            Statement stmt = dataSource.getConnection().createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stmt.executeQuery(query);
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
            loggerTBL.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public ResultSet findByAllPrSt(String query) {
        try {
            PreparedStatement stmt = dataSource.getConnection().prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stmt.executeQuery();
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
            loggerTBL.error(e.getMessage(), e);
            return null;
        }
    }


    @Override
    public DatabaseMetaData getMetaData() {
        return null;
    }

    @Override
    public String fieldsParameter(String query, String tableName) {
        return null;
    }

    @Override
    public void runnerToQuery(String query) {
        try {
            PreparedStatement st = dataSource.getConnection().prepareStatement(query);
            st.execute();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
            loggerTBL.error(e.getMessage(), e);
        }
    }

    @Override
    public Long getRowCount(String query) {
        try {
            Statement stmt = dataSource.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            Long count = rs.getLong(1);
            stmt.close();
            return count;
        } catch (SQLException e) {
            e.printStackTrace();
            loggerTBL.error(e.getMessage(), e);
            return -1L;
        }
    }

    public void insertBatchVarchar2(HashSet<String> tbl, String insertQuery, int iBatch) {

        if(tbl != null) {
            PreparedStatement st = null;
            try {
                st = dataSource.getConnection().prepareStatement(insertQuery);

                int countRec = 0;
                for (String row : tbl) {
                    //
                    st.setString(1, row);

                    //
                    st.addBatch();
                    countRec++;
                    if (countRec % iBatch == 0) {
                        System.out.println("Batch: " + countRec);
                        int[] updateBatch = st.executeBatch();
                        System.out.println("Batch to: " + updateBatch.length);
                        st.clearBatch();
                    }
                }
                //
                System.out.println("Insert Batch start");
                int[] updateBatch = st.executeBatch();
                //
                st.clearBatch();
                st.close();
                System.out.println("Insert Batch finish");
                loggerTBL.info("==Count Rec==: " + countRec);

            } catch (SQLException e) {
                e.printStackTrace();
                loggerTBL.error(e.getMessage(), e);
            }
        }
    }

}
